module.exports = {
  // https://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy
  // This option interrupts the configuration hierarchy at this file
  // Remove this if you have an higher level ESLint config file (it usually happens into a monorepos)
  root: true,

  parserOptions: {
    parser: '@babel/eslint-parser',
    ecmaVersion: 12, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
  },

  env: {
    browser: true,
    jest: true,
  },

  // Rules order is important, please avoid shuffling them
  extends: [
    // Base ESLint recommended rules
    // 'eslint:recommended',

    // Uncomment any of the lines below to choose desired strictness,
    // but leave only one uncommented!
    // See https://eslint.vuejs.org/rules/#available-rules
    // 'plugin:vue/essential', // Priority A: Essential (Error Prevention)
    // 'plugin:vue/strongly-recommended', // Priority B: Strongly Recommended (Improving Readability)
    'plugin:vue/recommended', // Priority C: Recommended (Minimizing Arbitrary Choices and Cognitive Overhead)

    'standard',

  ],

  plugins: [
    // https://eslint.vuejs.org/user-guide/#why-doesn-t-it-work-on-vue-file
    // required to lint *.vue files
    'vue',

  ],

  globals: {
    ga: true, // Google Analytics
    cordova: true,
    __statics: true,
    process: true,
    Capacitor: true,
    chrome: true,
  },

  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow paren-less arrow functions
    'arrow-parens': 'off',
    'one-var': 'off',
    'no-unused-vars': 'warn',

    'import/first': 'off',
    'import/named': 'error',
    'import/namespace': 'error',
    'import/default': 'error',
    'import/export': 'error',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
    'prefer-promise-reject-errors': 'off',

    // allow debugger during development only
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'no-console': 'warn',

    semi: [ 'error', 'always' ],
    quotes: [ 'error', 'single' ],
    indent: [
      'error',
      2,
      {
        SwitchCase: 1,
        FunctionDeclaration: { parameters: 'first' },
        FunctionExpression: { parameters: 'first' },
        CallExpression: { arguments: 'first' },
      },
    ],
    'vue/html-indent': [ 'error', 2 ],
    'no-var': 'error',
    'no-tabs': [ 'error' ],
    'no-trailing-spaces': 'error',
    'eol-last': [ 'error', 'always' ],
    'linebreak-style': [ 'error', 'unix' ],
    'one-var-declaration-per-line': [ 'error', 'always' ],
    'function-paren-newline': [ 'error', 'multiline' ],
    'object-curly-newline': [ 'error', { consistent: true } ],
    'space-unary-ops': 'error',
    'space-before-function-paren': 'error',
    'space-infix-ops': [ 'error', { int32Hint: false } ],
    'key-spacing': [ 'error', { afterColon: true } ],
    'keyword-spacing': 'error',
    'lines-between-class-members': [ 'error', 'always', { exceptAfterSingleLine: true } ],
    'brace-style': 'error',
    'array-element-newline': [ 'error', 'consistent' ],
    'comma-dangle': [ 'error', 'always-multiline' ],
    'comma-spacing': [
      'error', {
        before: false,
        after: true,
      },
    ],
    'no-extra-semi': 'error',
    'semi-spacing': [
      'error', {
        before: false,
        after: true,
      },
    ],
    'no-mixed-operators': [ 'error' ],
    'object-curly-spacing': [ 'error', 'always', { objectsInObjects: true } ],
    'array-bracket-spacing': [ 'error', 'always', { singleValue: true } ],
    'vue/max-attributes-per-line': [
      'error', {
        singleline: {
          max: 3,
        },
        multiline: {
          max: 3,
        },
      },
    ],
    'newline-before-return': 'error',
    'no-useless-catch': 'off',
    'default-case-last': 'off',
    'no-unused-expressions': 'error',
    'vue/multi-word-component-names': 'off'
  },
};
