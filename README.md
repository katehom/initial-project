# Quasar App (initial-project)

A Quasar Project

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Recommendations for the project initial setup
- shared components(in folder components) should be named AppMyComponent;
- Pages should be named MyPageName (for example ProfileSearch);
- Layouts should be named MyLayout (for example Header or MainLayout);
- All files with .vue should be multi files with structure as in example (src/components/link);
- Every component should have comments for the imports(example src/components/link):
```
// Quasar imports / Utils / Constants
/* There will be imports */

// Vuex
/* There will be imports */

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
  name: 'ComponentName',
  /* Component code */
}
```
- When you import styles, it should be scoped:
```
<style src="./AppLink.scss" lang="scss" scoped></style>
```
- All icons should be made with icon-font;
- Every route should be described in the separate file, files can be combined in 1 folder. For example in folder 'error-routes';
can be located 'not-found.route.js' and 'method-not-allowed.route.js'. Example you can see in the router folder;
- If you need to use some routes in Vue template (for example you want to add router-link or this.$router.push you shouldn't pass string there. Example how to use:
```
import { IndexPageRoute } from 'router/main-page-routes/index.route';

this.$router.push({ name: IndexPageRoute.name, params: { id: this.campaign.id } })
```
or
```
import { IndexPageRoute } from 'router/main-page-routes/index.route';

this.$router.push(IndexPageRoute.name)
```
- For the constants strings we should use 'config' folder. There can be located localstorage keys, roles, etc. If you have group of strings
with same idea (for example route permissions) please use object
```
export const ROUTE_PERMISSIONS = Object.freeze({
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
  CREATE: 'create',
});
```
- Please add lint-staged and husky to the project;
- Please take eslint and stylelint configs from the project example;
- Every prop in props field should have detailed description(type, required, default if not required)) :
```
props: {
  to: {
    type: Object,
    required: false,
    default: () => ({}),
  },
 }
```
- Lines endings - LF;
- Every module of store should be separated to the different folders and have 6 files (actions, getters, index, mutation-types, mutations, state);
