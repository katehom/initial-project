// Quasar imports / Utils / Constants
/* There will be imports */

// Vuex
/* There will be imports */

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
  name: 'AppLink',
  props: {
    to: {
      type: Object,
      required: false,
      default: () => ({}),
    },
    activeClass: {
      type: String,
      required: false,
      default: 'page-link--active',
    },
  },
};
