export const PASSWORD_GRANT_TYPE = 'password';
export const ACTIVE_PROFILE = 'active-profile';

export const AUTH_HEADER = {
  headers: {
    Authorization: true,
  },
};

// Localstorage keys
export const IS_FIRST_VOTING = 'isFirstVoting';
export const USER_TOKEN = 'user-token';
export const REFRESH_TOKEN = 'refresh-token';
export const SITE_LOCALE = 'site-locale';

export const ROUTE_PERMISSIONS = Object.freeze({
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
  CREATE: 'create',
});
