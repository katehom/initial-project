import { PageNotFoundRoute } from './not-found.route';

export const ErrorPageRoutes = [
  PageNotFoundRoute,
];
