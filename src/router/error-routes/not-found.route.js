const ErrorPageNotFound = () => import('pages/errors/Error404Page.vue');

export const PageNotFoundRoute = {
  name: 'PageNotFoundRoute',
  path: '*',
  component: ErrorPageNotFound,
};
