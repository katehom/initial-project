import { IndexPageRoute } from './index.route';

const MainPage = () => import('layouts/main-layout/MainLayout.vue');

const MainPageChildrenRoutes = [
  IndexPageRoute,
];

export const MainPageRoutes = {
  path: '/',
  component: MainPage,
  children: MainPageChildrenRoutes,
};
