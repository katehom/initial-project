const IndexPage = () => import('src/pages/Index.vue');

export const IndexPageRoute = {
  name: 'Main Page',
  path: '',
  component: IndexPage,
};
