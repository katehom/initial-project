import { MainPageRoutes } from './main-page-routes';
import { ErrorPageRoutes } from './error-routes';

const routes = [
  MainPageRoutes,
  // Always leave this as last one,
  ...ErrorPageRoutes,
];

export default routes;
